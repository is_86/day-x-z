#include <stdio.h>

#define TABSTOP 4
#define MAXTEXT 4000

void detab(char output[], int n) {

        for(int j = n; j < n+ TABSTOP; j++) {   
                output[j] = (' ');

        }
}

int main () {

        int c, i = 0, n = 0;
        char input[MAXTEXT];
        char output [MAXTEXT];
        printf("In my system tab is 4 spaces long.");
        while((c = getchar()) != EOF && n < MAXTEXT) {    
           if(c == '\t') {
                detab(output, n);
                n += 4;
           } else {
                output[n] = (c);
                n++;
           }
                input[i] = c;
                i++;        
        }

        for(int j = 0; j < i; j++) {
                putchar(input[j]);
        }

          for(int j = 0; j < n; j++) {
                putchar(output[j]);
        }

}


