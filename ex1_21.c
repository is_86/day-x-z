#include <stdio.h>

#define TABSTOP 4
#define MAXTEXT 4000

int  getLine(char input[]) {

        int c, i = 0;
        while((c = getchar()) != EOF && i < MAXTEXT) {
                input[i] = c;
                i++;
        }
        return i;
}

void replaceTabs(char input[], char output[]) {

        int p = 0,i = 0, mod = 0, h =0; 

        while(input[h] != '*') {
                while(input[h] == ' ') {
                        p++;
                        h++;
                        i++;
                }
                if(p>=TABSTOP) {
                        mod = p%TABSTOP;
                        while(p >= TABSTOP) {
                                if(p%4==0) {
                                        output[h] = '\t';
                                        h++;
                                }
                                p--;            
                        }
                        while(mod > 0) {
                                output[h] = ' ';
                                h++;
                        }

                } else {
                        while(p > 0) {
                                output[h] = ' ';
                                h++;
                                p--;
                        }
                }
                output[h] = input[i];
                h++;
                i++;
        }

}

int main() {
        int n, k = 0 ;
        char input[MAXTEXT];
        char output[MAXTEXT];

        for(int j = 0; j < MAXTEXT; j++) {
                input[j] = '*';
                output[j] = '*';
        }

        while(n = getLine(input) > 0);

        printf("%d\n", n);

        replaceTabs(input, output);

        printf("Modiffyed line\n");
         while(output[k] != '*') {
                printf("%c", output[k]); 
                k++;
        }

        printf("Original line\n");
        k = 0;
        while(input[k] != '*') {
                printf("%c", input[k]); 
                k++;
        }
}


