#include <stdio.h>


int main () {
        int lim = 100;
        printf("Program does for loop from page 41 without relational and logical operations in side for brackets. \n");
        int c,i =0,  n = lim -2;
        char s[lim-1];
        printf("Write something short here. Max length is 99 chars. End is EOF or newline\n");
        while(1) {
                if(n < 0) {
                        break;
                }
                c = getchar();
                if(c == EOF) {
                        break;
                }
                if(c == '\n') {
                        break;
                }
                s[i] = c;
                n--;
                i++;
        }

        printf("Lets print the table\n");

        for(int j = 0; j < i; j++) {
                printf("%c",s[j] );
        }
        printf("\n");
}

