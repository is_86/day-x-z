#include <stdio.h>
#include <ctype.h>

#define MAX 20

int any(char s [] , char s2 []) {
        int i =0, j=0, retval = -1;
        while(s[i] != '\0') {
                while(s2[j] != '\0') {
                        if(s[i] == s2[j]) {
                                retval = i;
                                break;
                        }
                        j++;
                }
                if(retval != -1) {
                        break;
                }
                i++;
        }
        return retval;

}

int main ()  {
        int c, n = 0, k = 0;

        char word[MAX];
        char word2[MAX];

        printf("Give  a word:\n");

        while((c = getchar()) != EOF && c != '\n') {
                word[n++] = c;
        }

        printf("Give second word:\n");

        while((c = getchar())!= EOF && c != '\n') {
                word2[k++] =c;

        }

        printf("Program gives the first index of the char which is same in first and second word. If no such it gives -1.\n");

        printf("Result: %d", any(word, word2));
}


