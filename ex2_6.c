#include <stdio.h>
#include <stdint.h>

#define mask 11111111

unsigned invert(unsigned x, int p, int n) {
        unsigned temp = x;     

        unsigned mask2 = mask >> (p);
        unsigned mask3 = mask << (8-(p +n));

        mask2 = mask2  & mask3;
        printf("%u\n", mask2);
        temp = temp & ~mask2;
        printf("%u\n", temp);
        return(temp);

}

unsigned setbits(unsigned x, int p, int n, unsigned y) {

        unsigned mask2 = mask >>(p);

        printf("%u\n", mask2);

        unsigned mask3 = mask << (8-(p +n));

        mask2 = mask2  & mask3;
        printf("%u\n", mask2);
        return( (y  & mask2) | x);
}


int main ()  {

        printf("binary numbers, max is 11111111. setbits(34, 5, 2, 58); %u\n", setbits(34,5,2,58));

        printf("17, 39 4, 3. Tässä :  %u\n", setbits(17, 4, 3, 39));

        printf("24, 46, 4, 2 Tässä: %u\n", setbits(24, 4, 2, 46));

         printf("Invert n bits from position p 23, 5, 2: %u, 57, 1, 2: %u  195,6, 1: %u", invert(23,5,2), invert(57,1,2), invert(195, 6, 1));

}


