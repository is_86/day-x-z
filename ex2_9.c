#include <stdio.h>


int  bitcount (unsigned x) {
        int n = 0;

        while(x != 0) {
                x &= (x -1);
                n++;
        }
        return n;
}


int main() {
        printf("The expression in while takes last 1-bit away (becuase it is the smalles one to take something away).\n Right to that would be some ones but there is this &= which stops them to come along.\n");

        printf ("Bitcount of number 23: %d, 56: %d, 124: %d \n", bitcount(23), bitcount(56), bitcount(124));
}


